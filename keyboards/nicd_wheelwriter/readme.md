IBM Wheelwriter
===============

IBM Wheelwriter 6 series keyboard.

Keyboard Maintainer: Nicd
Hardware Supported: IBM Wheelwriter 6 series
Hardware Availability: Check eBay

Make example for this keyboard (after setting up your build environment):

    make nicd_wheelwriter:default

See [build environment setup](https://docs.qmk.fm/build_environment_setup.html) then the [make instructions](https://docs.qmk.fm/make_instructions.html) for more information.
