#include "nicd_wheelwriter.h"

const uint16_t keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
      Layer 0: Default Layer (resembling ISO QWERTY)
      L1 switches to Layer 1 when held
      ----------------------------------------------------------------------------
      |ESC|VMT|-|GRV| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 |MNS|EQL|BCKSPC|-|UP |
      ----------------------------------------------------------------------------
      |VDN|VUP|-|TAB | Q | W | E | R | T | Y | U | I | O | P |LBR|RBR|ENTER|-|DNW|
      -----------------------------------------------------------------    -------
      |ALT|---|-|CAPS | A | S | D | F | G | H | J | K | L |SCL|QUO|NUH|    |-|LFT|
      ----------------------------------------------------------------------------
      |SLC|PAU|-|LSF|NUB| Z | X | C | V | B | N | M |COM|DOT|SLS|      LSFT|-|RGT|
      ----------------------------------------------------------------------------
      |L1 |LCT|----------|   LGUI   |            SPC            |-----|RALT|-|RCT|
      ----------------------------------------------------------------------------

      Notice both shifts are mapped to left shift since they are the same character in the matrix.

      Layer 1: F keys, page keys, delete
      ----------------------------------------------------------------------------
      |   |   |-|   |F1 |F2 |F3 |F4 |F5 |F6 |F7 |F8 |F9 |F10|F11|F12|   DEL|-|PGU|
      ----------------------------------------------------------------------------
      |   |   |-|    |   |   |   |   |   |   |   |   |   |   |   |   |     |-|PGD|
      -----------------------------------------------------------------    -------
      |   |   |-|     |   |   |   |   |   |   |   |   |   |   |   |   |    |-|HOM|
      ----------------------------------------------------------------------------
      |   |   |-|   |   |   |   |   |   |   |   |   |   |   |   |          |-|END|
      ----------------------------------------------------------------------------
      |   |   |----------|          |                           |-----|    |-|   |
      ----------------------------------------------------------------------------
     */
    [0] = KEYMAP(
      KC_ESC, KC_MUTE, KC_GRV, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC, KC_UP,
      KC_VOLD, KC_VOLU, KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_ENT, KC_DOWN,
      KC_LALT, KC_NO, KC_CAPS, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_NUHS, KC_LEFT,
      KC_SLCK, KC_BRK, KC_LSFT, KC_NUBS, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RIGHT,
      MO(1), KC_LCTL, KC_LGUI, KC_SPC, KC_RALT, KC_RCTL
    ),

    [1] = KEYMAP(
      KC_TRNS, KC_TRNS, KC_TRNS, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_DEL, KC_PGUP,
      KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_PGDN,
      KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_HOME,
      KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_END,
      KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS
    ),
};

const uint16_t fn_actions[] = {
    
};

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {

};

// Runs constantly in the background, in a loop.
void matrix_scan_user(void) {

};

bool process_record_user(uint16_t keycode, keyrecord_t *record)
{
    matrix_print();
    return true;
}
