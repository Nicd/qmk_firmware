#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "hal.h"
#include "timer.h"
#include "wait.h"
#include "print.h"
#include "matrix.h"


/* matrix state(1:on, 0:off) */
static matrix_row_t matrix[MATRIX_ROWS];
static matrix_row_t matrix_debouncing[MATRIX_ROWS];
static bool debouncing = false;
static uint16_t debouncing_time = 5;


/*
GPIO_Pin Matrix_cols[] = {
                // Teensy pin #
  gpio(B, 16),  // 0
  gpio(B, 17),  // 1
  gpio(D, 0),   // 2
  gpio(A, 12),  // 3
  gpio(A, 13),  // 4
  gpio(D, 7),   // 5
  gpio(D, 4),   // 6
  gpio(D, 2),   // 7
  gpio(D, 3),   // 8
  gpio(C, 3),   // 9
  gpio(C, 4),   // 10
  gpio(C, 6),   // 11
  gpio(C, 7),   // 12
};

GPIO_Pin Matrix_rows[] = {
  gpio(D, 6), // 21
  gpio(D, 5), // 20
  gpio(B, 2), // 19
  gpio(B, 3), // 18
  gpio(B, 1), // 17
  gpio(B, 0), // 16
  gpio(C, 0), // 15
  gpio(D, 1), // 14
};
*/

#define INITCOL(PINCHAR, PINNO) palSetPadMode(GPIO ## PINCHAR, PINNO, PAL_MODE_INPUT_PULLDOWN)
#define INITROW(PINCHAR, PINNO) palSetPadMode(GPIO ## PINCHAR, PINNO, PAL_MODE_OUTPUT_PUSHPULL)

void matrix_init(void)
{
    /* Column(sense) */
    INITCOL(B, 16);
    INITCOL(B, 17);
    INITCOL(D, 0);
    INITCOL(A, 12);
    INITCOL(A, 13);
    INITCOL(D, 7);
    INITCOL(D, 4);
    INITCOL(D, 2);
    INITCOL(D, 3);
    INITCOL(C, 3);
    INITCOL(C, 4);
    INITCOL(C, 6);
    INITCOL(C, 7);

    /* Row(strobe) */
    INITROW(D, 6);
    INITROW(D, 5);
    INITROW(B, 2);
    INITROW(B, 3);
    INITROW(B, 1);
    INITROW(B, 0);
    INITROW(C, 0);
    INITROW(D, 1);

    memset(matrix, 0, MATRIX_ROWS * sizeof(matrix_row_t));
    memset(matrix_debouncing, 0, MATRIX_ROWS * sizeof(matrix_row_t));

    matrix_init_quantum();
}

uint8_t matrix_scan(void)
{
    for (int row = 0; row < MATRIX_ROWS; row++) {
        matrix_row_t data = 0;
        // strobe row
        switch (row) {
            case 0: palSetPad(GPIOD, 6);    break;
            case 1: palSetPad(GPIOD, 5);    break;
            case 2: palSetPad(GPIOB, 2);    break;
            case 3: palSetPad(GPIOB, 3);    break;
            case 4: palSetPad(GPIOB, 1);    break;
            case 5: palSetPad(GPIOB, 0);    break;
            case 6: palSetPad(GPIOC, 0);    break;
            case 7: palSetPad(GPIOD, 1);    break;
        }

        // need wait to settle pin state
        // if you wait too short, or have a too high update rate
        // the keyboard might freeze, or there might not be enough
        // processing power to update the LCD screen properly.
        // 20us, or two ticks at 100000Hz seems to be OK
        wait_us(20);

        // read col data
        data = (
            (palReadPad(GPIOB, 16)  << 0 )  |
            (palReadPad(GPIOB, 17)  << 1 )  |
            (palReadPad(GPIOD, 0)   << 2 )  |
            (palReadPad(GPIOA, 12)  << 3 )  |
            (palReadPad(GPIOA, 13)  << 4 )  |
            (palReadPad(GPIOD, 7)   << 5 )  |
            (palReadPad(GPIOD, 4)   << 6 )  |
            (palReadPad(GPIOD, 2)   << 7 )  |
            (palReadPad(GPIOD, 3)   << 8 )  |
            (palReadPad(GPIOC, 3)   << 9 )  |
            (palReadPad(GPIOC, 4)   << 10 ) |
            (palReadPad(GPIOC, 6)   << 11 ) |
            (palReadPad(GPIOC, 7)   << 12 )
        );

        // un-strobe row
        switch (row) {
            case 0: palClearPad(GPIOD, 6);    break;
            case 1: palClearPad(GPIOD, 5);    break;
            case 2: palClearPad(GPIOB, 2);    break;
            case 3: palClearPad(GPIOB, 3);    break;
            case 4: palClearPad(GPIOB, 1);    break;
            case 5: palClearPad(GPIOB, 0);    break;
            case 6: palClearPad(GPIOC, 0);    break;
            case 7: palClearPad(GPIOD, 1);    break;
        }

        if (matrix_debouncing[row] != data) {
            matrix_debouncing[row] = data;
            debouncing = true;
            debouncing_time = timer_read();
        }
    }

    if (debouncing && timer_elapsed(debouncing_time) > DEBOUNCE) {
        for (int row = 0; row < MATRIX_ROWS; row++) {
            matrix[row] = matrix_debouncing[row];
        }
        debouncing = false;
    }
    matrix_scan_quantum();
    return 1;
}

bool matrix_is_on(uint8_t row, uint8_t col)
{
    return (matrix[row] & (1<<col));
}

matrix_row_t matrix_get_row(uint8_t row)
{
    return matrix[row];
}

void matrix_print(void)
{
    xprintf("\nr/c 0123456789ABC\n");
    for (uint8_t row = 0; row < MATRIX_ROWS; row++) {
        xprintf("%02X: ", row);
        matrix_row_t data = matrix_get_row(row);
        for (int col = 0; col < MATRIX_COLS; col++) {
            if (data & (1<<col))
                xprintf("1");
            else
                xprintf("0");
        }
        xprintf("\n");
    }
}
