/*
Copyright 2014 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef KEYMAP_COMMON_H
#define KEYMAP_COMMON_H

#include "quantum.h"

#define LAYOUT_wheelwriter_6 KEYMAP

/*
    WHEELWRITER KEYMAP
    This keymap maps the matrix and its key positions in IBM Wheelwriter series 6 keyboard.

    Format is (col,row)

    ----------------------------------------------------------------------------
    |1,2|2,2|-|4,2|4,3|5,3|6,3|7,3|7,2|8,2|8,3|9,3|A,3|B,3|B,2|9,2|   C,2|-|C,3|
    ----------------------------------------------------------------------------
    |1,3|1,4|-|2,4 |4,4|5,4|6,4|7,4|7,5|8,5|8,4|9,4|A,4|B,4|B,5|9,5|  C,6|-|C,4|
    -----------------------------------------------------------------    -------
    |1,5|2,5|-|2,6  |4,1|5,1|6,1|7,1|7,0|8,0|8,1|9,1|A,1|B,1|B,0|B,6|    |-|C,1|
    ----------------------------------------------------------------------------
    |1,1|2,1|-|3,7|4,7|4,6|5,6|6,6|7,6|7,7|8,7|8,6|9,6|A,6|B,7|       3,7|-|1,6|
    ----------------------------------------------------------------------------
    |2,0|1,0|----------|   0,7    |            1,7            |-----| 2,7|-|C,7|
    ----------------------------------------------------------------------------

    Notice both left and right shift are the same (3,7), they are wired to the same pins.
*/

#define NOP KC_NO

// These match the map above
#define KEYMAP( \
    K12, K22,   K42, K43, K53, K63, K73, K72, K82, K83, K93, KA3, KB3, KB2, K92, KC2,   KC3, \
    K13, K14,   K24, K44, K54, K64, K74, K75, K85, K84, K94, KA4, KB4, KB5, K95, KC6,   KC4, \
    K15, K25,   K26, K41, K51, K61, K71, K70, K80, K81, K91, KA1, KB1, KB0, KB6,        KC1, \
    K11, K21,   K37, K47, K46, K56, K66, K76, K77, K87, K86, K96, KA6, KB7,             K16, \
    K20, K10,             K07,                K17,                               K27,   KC7  \
) { \
    { NOP, K10, K20, NOP, NOP, NOP, NOP, K70, K80, NOP, NOP, KB0, NOP }, \
    { NOP, K11, K21, NOP, K41, K51, K61, K71, K81, K91, KA1, KB1, KC1 }, \
    { NOP, K12, K22, NOP, K42, NOP, NOP, K72, K82, K92, K93, KB2, KC2 }, \
    { NOP, K13, NOP, NOP, K43, K53, K63, K73, K83, K93, KA3, KB3, KC3 }, \
    { NOP, K14, K24, NOP, K44, K54, K64, K74, K84, K94, KA4, KB4, KC4 }, \
    { NOP, K15, K25, NOP, NOP, NOP, NOP, K75, K85, K95, NOP, KB5, NOP }, \
    { NOP, K16, K26, NOP, K46, K56, K66, K76, K86, K96, KA6, KB6, KC6 }, \
    { K07, K17, K27, K37, K47, NOP, NOP, K77, K87, NOP, NOP, KB7, KC7 } \
}

#endif
